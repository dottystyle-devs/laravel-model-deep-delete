<?php

namespace Dottystyle\LaravelModelDeepDelete;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\Relation;

trait DeepDeletes
{
    /**
     * Deeply delete the model.
     * 
     * @return bool
     */
    public function deleteDeep()
    {
        $deletable = $this->getDeeplyDeletableItems();
        
        // Try to load relations first.
        if ($relations = array_filter($deletable, 'is_string')) {
            $this->load($relations);
        }

        foreach ($deletable as $deletable) {
            $this->deeplyDeleteDeletable($deletable);
        }

        $this->delete();

        return true;
    }

    /**
     * Delete the deletable based on its data type.
     * 
     * A deletable can be one of the following:
     *   - string (relation name)
     *   - model collection
     *   - eloquent builder
     *   - relationship object
     *   - callable that returns one of those type
     * 
     * @param mixed $deletable
     * @return void
     */
    protected function deeplyDeleteDeletable($deletable)
    {
        if (is_string($deletable) && $this->relationLoaded($deletable)) {
            $this->deeplyDeleteRelated($deletable);
        } elseif ($deletable instanceof Collection) {
            $this->deeplyDeleteMany($deletable);
        } elseif ($deletable instanceof Relation) {
            $deletable->delete();
        } elseif ($deletable instanceof Builder) {
            $this->deeplyDeleteMany($deletable->get());
        } elseif (is_callable($deletable)) {
            // The return value of a callable deleteable might be another deletable.
            $result = call_user_func($deletable, $this);

            if (! is_null($result)) {
                $this->deeplyDeleteDeletable($result);
            }
        }
    }

    /**
     * Deeply delete the related models given the relation name.
     * 
     * @param string $relation
     * @return void
     */
    protected function deeplyDeleteRelated($relation)
    {
        $related = $this->getRelation($relation);

        $this->deeplyDeleteMany($related instanceof Collection ? $related->all() : [$related]);
    }

    /**
     * Deeply delete a list or collection of models.
     * 
     * @param mixed $models
     * @return void
     */
    protected function deeplyDeleteMany($models)
    {
        foreach ($models as $model) {
            if ($model instanceof DeeplyDeletable) {
                $model->deepDelete();
            } else {
                $model->delete();
            }
        }
    }

    /**
     * Get the related models to be deleted.
     * 
     * @return void
     */
    public function getDeeplyDeletableItems()
    {
        return [];
    }
}

<?php

namespace Dottystyle\LaravelModelDeepDelete;

use Illuminate\Database\Eloquent\Builder;

interface DeeplyDeletable
{
    /**
     * Deeply delete the model.
     * 
     * @return bool
     */
    public function deleteDeep();

    /**
     * Get the related models to be deleted.
     * 
     * @return array
     */
    public function getDeeplyDeletableItems();
}
